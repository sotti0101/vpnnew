package android.net.vpn;
//http://www.javased.com/index.php?source_dir=android-vpn-settings/src/com/android/settings/vpn/VpnProfileActor.java

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//###########################################################################///
        VpnProfile p = new PptpProfile();
        p.setName("testi");
        p.setServerName("193.242.195.11");
        p.setId("1212");
        p.setDomainSuffices("193.242.195.11");
        p.setRouteList("0.0.0.0");
        p.setSavedUsername("iman");
        p.setState(VpnState.IDLE);
        Log.i("mamal","salam zesh");

        IVpnService con = new IVpnService() {
            @Override
            public boolean connect(VpnProfile profile, String username, String password) throws RemoteException {
                return false;
            }

            @Override
            public void disconnect() throws RemoteException {

            }

            @Override
            public void checkStatus(VpnProfile profile) throws RemoteException {

            }

            @Override
            public IBinder asBinder() {
                return null;
            }
        };

         boolean NAME = false;
        Log.i("aaa", String.valueOf(NAME));
        try {
            NAME =  con.connect(p,"iman","4414");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        Log.i("aaa", String.valueOf(NAME));

        //testVpnType(VpnType.PPTP);
        //assertFalse(TextUtils.isEmpty(VpnType.PPTP.getDisplayName()));
        //assertNotNull(VpnType.PPTP.getProfileClass());
//#############################################################################///
        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
