#include <jni.h>
#include <string>

const char *returnValue;

JNIEXPORT jstring JNICALL
Java_android_os_SystemProperties_native_1get__Ljava_lang_String_2(JNIEnv *env, jclass type,
                                                                  jstring key_) {
    const char *key = env->GetStringUTFChars(key_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);

    return env->NewStringUTF(returnValue);
}

JNIEXPORT jstring JNICALL
Java_android_os_SystemProperties_native_1get__Ljava_lang_String_2Ljava_lang_String_2(JNIEnv *env,
                                                                                     jclass type,
                                                                                     jstring key_,
                                                                                     jstring def_) {
    const char *key = env->GetStringUTFChars(key_, 0);
    const char *def = env->GetStringUTFChars(def_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);
    env->ReleaseStringUTFChars(def_, def);

    return env->NewStringUTF(returnValue);
}

JNIEXPORT jint JNICALL
Java_android_os_SystemProperties_native_1get_1int(JNIEnv *env, jclass type, jstring key_,
                                                  jint def) {
    const char *key = env->GetStringUTFChars(key_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);
}

JNIEXPORT jlong JNICALL
Java_android_os_SystemProperties_native_1get_1long(JNIEnv *env, jclass type, jstring key_,
                                                   jlong def) {
    const char *key = env->GetStringUTFChars(key_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);
}

JNIEXPORT jboolean JNICALL
Java_android_os_SystemProperties_native_1get_1boolean(JNIEnv *env, jclass type, jstring key_,
                                                      jboolean def) {
    const char *key = env->GetStringUTFChars(key_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);
}

JNIEXPORT void JNICALL
Java_android_os_SystemProperties_native_1set(JNIEnv *env, jclass type, jstring key_, jstring def_) {
    const char *key = env->GetStringUTFChars(key_, 0);
    const char *def = env->GetStringUTFChars(def_, 0);

    // TODO

    env->ReleaseStringUTFChars(key_, key);
    env->ReleaseStringUTFChars(def_, def);
}

extern "C"
JNIEXPORT jstring JNICALL
Java_android_net_vpn_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
